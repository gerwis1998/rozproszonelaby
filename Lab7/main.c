#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

#define THREADSLIMIT 8

int *primeNumbers;
int currentNumber = 0;
pthread_mutex_t mutex;

void addToPrimeNumbers(int);

void *isPrimeNumber(int);

int main() {
    int minimalPrimeNumber;
    int maximalPrimeNumber;

    printf("Podaj liczbe poczatkowa: ");
    scanf("%d", &minimalPrimeNumber);

    printf("Podaj liczbe koncowa: ");
    scanf("%d", &maximalPrimeNumber);

    primeNumbers = malloc(sizeof(int)*maximalPrimeNumber-minimalPrimeNumber);
    pthread_mutex_init(&mutex, NULL);
    pthread_t threads[THREADSLIMIT];

    int activeThreadsCounter = 0;
    for (int currentNumber = minimalPrimeNumber; currentNumber <= maximalPrimeNumber; currentNumber++) {
        pthread_create(&threads[activeThreadsCounter], NULL, isPrimeNumber, currentNumber);
        activeThreadsCounter++;

        if (activeThreadsCounter == THREADSLIMIT) {
            for (int j =0; j < THREADSLIMIT; j++) {
                pthread_join(threads[j], NULL);
            }
            activeThreadsCounter = 0;
        }
    }

    if (activeThreadsCounter != 0) {
        for (int j =0; j < activeThreadsCounter; j++) {
            pthread_join(threads[j], NULL);
        }
    }

    printf("Liczby pierwsze w danym zakresie to:\n");
    for (int i=0; i<currentNumber; i++) {
        printf("%d\n", primeNumbers[i]);
    }
}

void addToPrimeNumbers(int num) {
    pthread_mutex_lock(&mutex);
    primeNumbers[currentNumber] = num;
    currentNumber++;
    pthread_mutex_unlock(&mutex);
}

void *isPrimeNumber(int numberToCheck) {
    int isPrime = 1;

    if (numberToCheck == 0 || numberToCheck == 1) return 0;

    for (int j=2; j*j <= numberToCheck; j++) {
        if (numberToCheck%j == 0) {
            isPrime = !isPrime;
            break;
        }
    }

    if (isPrime) addToPrimeNumbers(numberToCheck);
    return 0;
}