#include <iostream>
#include <windows.h>


long FAR PASCAL
WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam){
	COPYDATASTRUCT* pcds = (COPYDATASTRUCT*)lParam;
	switch (uMsg){
	case WM_COPYDATA:
		if (pcds->dwData == 100){
			char* lpszString = (char*)(pcds->lpData);
			std::cout << lpszString << std::endl;
		}
		break;
	default:
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}
	return (LRESULT)NULL;
}


int main()
{
	char t[256] = "Receiver";
	SetConsoleTitleA(t);
	const HWND hwndConsole = ::FindWindowExA(0, 0, "Receiver", 0);
	const HINSTANCE hInstance = (HINSTANCE)GetWindowLong(hwndConsole, GWL_HINSTANCE);
	WNDCLASS wndClass = { 0 };
	wndClass.hInstance = hInstance;
	wndClass.lpfnWndProc = WndProc;
	wndClass.lpszClassName = TEXT("clipboardreceiver");

	if (!RegisterClass(&wndClass)) return 1;

	const HWND hwndWindow = CreateWindow(TEXT("clipboardreceiver"),
		TEXT(""),
		WS_MINIMIZE,
		520, 20, 100, 100,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwndWindow, SW_SHOWMINIMIZED);
	UpdateWindow(hwndWindow);

	MSG msg;
	while (GetMessage(&msg, hwndWindow, 0, 0)){
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}