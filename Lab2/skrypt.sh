#!/bin/bash
rm -rf lib_List2.a lib_List.so
gcc -c List2.c -o List2.o
ar r lib_List2.a List2.o
gcc -c -Wall -fPIC -D_GNU_SOURCE List.c
gcc List.o -shared -o lib_List.so
gcc -c Source.c -o Source.o
gcc Source.o lib_List2.a -L. -l_List -o Source.out
rm -rf List2.o List.o Source.o
