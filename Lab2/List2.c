//
// Created by GerWis on 04.03.19.
//
#include <malloc.h>
#include "List.h"
bool insert(List *list, int val, unsigned int pos) {
    if (pos < 0 || pos > list->size) return false;
    if (pos == 0)
    {
        push(list, val);
        return true;
    }
    Node *temp = list->head;
    for (unsigned int i = 0; i < pos; i++) temp = temp->next;
    Node *to_insert = getNewNode(val);
    to_insert->prev = temp->prev;
    Node *temp2 = temp->prev;
    temp2->next = to_insert;
    to_insert->next = temp;
    temp->prev = to_insert;
    list->size++;
    return true;
}

bool removeElement(List *list, unsigned int pos) {
    if (pos < 0 || pos >= list->size) return false;
    if (list->size == 1){
        pop(list);
        return true;
    }
    Node *temp = list->head;
    for (unsigned int i = 0; i < pos; i++) temp = temp->next;
    Node *prev = temp->prev;
    Node *next = temp->next;
    prev->next = next;
    next->prev = prev;
    free(temp);
    if (pos == 0) list->head = next;
    if (pos == list->size - 1) list->tail = prev;
    list->size--;
}