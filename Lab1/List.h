#pragma once
#include <stdbool.h>

typedef struct {
	int value;
	struct Node* next;
	struct Node* prev;
}Node;

typedef struct {
	Node* head;
	Node* tail;
	unsigned int size;
}List;

List *getNewList();

Node *getNewNode(int value);

void push(List *list, int value);

bool pop(List *list);

void print(List *list, bool backwards);

bool insert(List *list, int val, unsigned int pos);

bool removeElement(List *list, unsigned int pos);