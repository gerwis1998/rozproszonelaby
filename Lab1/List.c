#include "List.h"
#include <stdio.h>
#include <malloc.h>

List *getNewList()
{
	List *temp = malloc(sizeof(List));
	temp->head = NULL;
	temp->tail = NULL;
	temp->size = 0;
	return temp;
}

Node *getNewNode(int value)
{
	Node* newNode = malloc(sizeof(Node));
	newNode->value = value;
	newNode->next = NULL;
	newNode->prev = NULL;
	return newNode;
}

void push(List *list, int value)
{
	Node* newNode = getNewNode(value);
	if (list->size == 0)
	{
		list->head = newNode;
		list->tail = newNode;
		newNode->prev = newNode;
		newNode->next = newNode;
		list->size = 1;
	}
	else
	{
		newNode->prev = list->tail;
		newNode->next = list->head;
		list->tail->next = newNode;
		list->head->prev = newNode;
		list->head = newNode;
		list->size++;
	}
}

bool pop(List *list)
{
	if (list->head == NULL)
	{
		return false;

	}
	Node* temp = list->head;
	int value = list->head->value;
	if (list->head->prev == list->head->next)
	{
		free(temp);
		list->head = NULL;
		list->tail = NULL;
		list->size--;
		return true;
	}
	list->head = list->head->next;
	list->head->prev = list->tail;
	free(temp);
	list->size--;
	return true;
}

void print(List *list, bool backwards)
{
	if (list->head != NULL)
	{

		if (!backwards)
		{
			Node *temp = list->head;
			for (int i = 0; i < list->size; i++)
			{
				printf("%d, ", temp->value);
				temp = temp->next;
			}
			printf("\n");
		}
		else
		{
			Node *temp = list->tail;
			for (int i = 0; i < list->size; i++)
			{
				printf("%d, ", temp->value);
				temp = temp->prev;
			}
			printf("\n");
		}
	}
}

bool insert(List *list, int val, unsigned int pos) {
	if (pos < 0 || pos > list->size) return false;
	if (pos == 0)
	{
		push(list, val);
		return true;
	}
	Node *temp = list->head;
	for (unsigned int i = 0; i < pos; i++) temp = temp->next;
	Node *to_insert = getNewNode(val);
	to_insert->prev = temp->prev;
	Node *temp2 = temp->prev;
	temp2->next = to_insert;
	to_insert->next = temp;
	temp->prev = to_insert;
	list->size++;
	return true;
}

bool removeElement(List *list, unsigned int pos) {
	if (pos < 0 || pos >= list->size) return false;
	if (list->size == 1){
		pop(list);
		return true;
	}
	Node *temp = list->head;
	for (unsigned int i = 0; i < pos; i++) temp = temp->next;
	Node *prev = temp->prev;
	Node *next = temp->next;
	prev->next = next;
	next->prev = prev;
	free(temp);
	if (pos == 0) list->head = next;
	if (pos == list->size - 1) list->tail = prev;
	list->size--;
}