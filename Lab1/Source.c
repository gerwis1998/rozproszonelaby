#include "List.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main() {
	List *list = getNewList();
	char wyjscie[] = "q";
	char push_s[] = "push";
	char pop_s[] = "pop";
	char insert_s[] = "insert";
	char remove_s[] = "remove";
	char print_s[] = "print";
	char true_s[] = "true";
	char false_s[] = "false";
	char input[256];
	int value;
	int pos;
	printf("Wyjscie: q\nKomendy:\npush wartosc\npop\ninsert wartosc pozycja\nremove pozycja\nprint backwards(true/false)\n");
	do{
		fflush(stdout);
		scanf("%255s", input, 256);
		if(strcmp(input, push_s) == 0)
		{
			scanf("%d", &value);
			push(list, value);
		}
		else if(strcmp(input, pop_s) == 0) pop(list);
		else if(strcmp(input, insert_s) == 0)
		{
			scanf("%d%d", &value, &pos);
			insert(list, value, pos);
		}
		else if(strcmp(input, remove_s) == 0)
		{
			scanf("%d", &pos);
			removeElement(list, pos);
		}
		else if(strcmp(input, print_s) == 0)
		{
			scanf("%255s", input, 256);
			if(strcmp(input,true_s) == 0) print(list, true);
			else if(strcmp(input, false_s) == 0) print(list, false);
		}
	} while (strcmp(input, wyjscie)!=0);
	return 0;
}